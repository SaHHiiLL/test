
CFLAGS=-std=c++20 -shared -fPIC
CC=clang
BUILD_DIR=target

build: build_dir
	$(CC) $(CFLAGS) -o $(BUILD_DIR)/libhello.so src/*.cpp

build_dir:
	mkdir -p $(BUILD_DIR)

