#include <iostream>
#include <hyprland/src/plugins/PluginAPI.hpp>

inline HANDLE PHANDLE = nullptr;

APICALL EXPORT std::string PLUGIN_API_VERSION() 
{
    return HYPRLAND_API_VERSION;
}

APICALL EXPORT PLUGIN_DESCRIPTION_INFO PLUGIN_INIT(HANDLE handle) {
    PHANDLE = handle;

    // ...
    // APICALL bool addNotification(HANDLE handle, const std::string& text, const CColor& color, const float timeMs);

    HyprlandAPI::addNotification(PHANDLE, std::string("Hello worl from plug"), CColor(0xff1100ff), (float)60000);
    std::cout << "Hello world" << std::endl;


    return {"MyPlugin", "An amazing plugin that is going to change the world!", "Me", "1.0"};
}

APICALL EXPORT void PLUGIN_EXIT() {
    // ...
    std::cout << "Exiting plugin..." << std::endl;
}
